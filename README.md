# myinv

ansible mariadb dynamic inventory

# dependencies

* `pip install ansible --user`
* `pip install mariasql --user`
* mariadb >= 10.3

# `~/.myinv.json`

```json
{
    "MARIADB_HOST": "my_mariadb_host", 
    "MARIADB_PORT": 3306,
    "MARIADB_USER": "my_user",
    "MARIADB_PASSWORD": "my_password",
    "MARIADB_DATABASE": "ansible"    
}
```
