#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import os
import mariasql
import pprint
import sys
import boto3

# read config (json format) from file
try:
    config = json.load(open(os.getenv("HOME") + "/.myinv.json"))
except:
    sys.exit(1)


def get_inventory(hostgroup = None):

    db = mariasql.MariaSQL(host=config["MARIADB_HOST"],
                            port=config["MARIADB_PORT"],
                            user=config["MARIADB_USER"],
                            password=config["MARIADB_PASSWORD"],
                            db=config["MARIADB_DATABASE"])

    SQL = """
with v as (
select
    groups.name,
    CONCAT ('"vars": {',
    GROUP_CONCAT('"',
    vars.name,
    '": "',
    vars.value,
    '"'),
    '}') as vars
from
    groups
inner join vars on
    (JSON_SEARCH(vars.groups, 'one', groups.name) is not null)
group by
    groups.name),
h as (
select
    groups.name,
    CONCAT ('"hosts": [',
    GROUP_CONCAT('"',
    hosts.name,
    '"'),
    ']') as hosts
from
    groups
inner join hosts on
    (JSON_SEARCH(hosts.groups, 'one', groups.name) is not null)
group by
    groups.name ) select
    CONCAT('{"',
    h.name,
    '":{',
    h.hosts,
    ',',
    v.vars,
    '}}') as inventory
from
    h
inner join v on
    (v.name = h.name)
"""

    if hostgroup is not None:
        SQL = SQL + " where h.name = '{name}';".format(name = hostgroup)

    raw = db.query(SQL)
    inventory = dict()

    for inv in raw:
        inventory.update(json.loads(inv['inventory']))

    print(json.dumps(inventory))


if __name__ == '__main__':

    if len(sys.argv) == 2:
        if sys.argv[1] == "--list":
            get_inventory()
    elif len(sys.argv) == 3:
        if sys.argv[1] == "--host":
           get_inventory(sys.argv[2])
