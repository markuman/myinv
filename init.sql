DROP TABLE IF EXISTS `cmdb_hosts`;

CREATE TABLE `cmdb_hosts` (
  `name` varchar(255) DEFAULT NULL,
  `fqdn` varchar(255) DEFAULT NULL,
  `main_ip` varchar(15) DEFAULT NULL,
  `os_name` varchar(80) DEFAULT NULL,
  `os_version` varchar(40) DEFAULT NULL,
  `system` varchar(40) DEFAULT NULL,
  `kernel` varchar(40) DEFAULT NULL,
  `arch_hardware` varchar(12) DEFAULT NULL,
  `arch_userspace` varchar(12) DEFAULT NULL,
  `virt_type` varchar(20) DEFAULT NULL,
  `virt_role` varchar(20) DEFAULT NULL,
  `cpu_type` varchar(60) DEFAULT NULL,
  `vcpus` int(11) DEFAULT NULL,
  `ram` float DEFAULT NULL,
  `disk_total` float DEFAULT NULL,
  `disk_free` float DEFAULT NULL,
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 WITH SYSTEM VERSIONING;


DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `children` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`idx`),
  KEY `groups` (`children`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 WITH SYSTEM VERSIONING;

DROP TABLE IF EXISTS `hosts`;

CREATE TABLE `hosts` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `groups` tinytext DEFAULT NULL,
  `notes` tinytext DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 WITH SYSTEM VERSIONING;

DROP TABLE IF EXISTS `vars`;

CREATE TABLE `vars` (
  `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `groups` tinytext DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 WITH SYSTEM VERSIONING;

